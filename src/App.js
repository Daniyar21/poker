import './App.css';
import {Component} from "react";
import Cards from "./Cards/Cards";

const getRandomNumber = max =>{
    return Math.floor(Math.random()*max)+1;
}

class CardDeck {
    constructor() {
        const cardRanks = ['2','3','4','5','6','7','8','9','10','J','Q','K','A',];
        const cardSuits = ['H', 'S', 'D', 'C'];

        const cards = [];
        for (let i=0; i<cardSuits.length; i++) {
            for (let j=0; j<cardRanks.length; j++) {
                const cardObj = {suit: cardSuits[i], ranks: cardRanks[j]};
                cards.push(cardObj);
            }
        }
        this.cards = cards;
    }

getCard = () => {
    const maxCardNumber = this.cards.length-1;
    const cardIndex = getRandomNumber(maxCardNumber);
    const [oneCard] = this.cards.splice(cardIndex, 1);

    return oneCard;
}

getCards = howMany => {
        const fiveCards = [];
        for (let i=1; i <= howMany; i++) {
            const oneCard = this.getCard();
            fiveCards.push(oneCard);
        }
        return fiveCards;
}
}
class PokerHand{
    constructor(arr) {
        const combination =[];
        for (let i = 0; i <arr.length ; i++) {
           combination.push(arr[i].ranks);
        }
        this.hands = combination.join('');
        console.log(this.hands);
    }

    getOutcome =()=>{
        const combinationStr = this.hands;
        console.log(combinationStr);

       const getRepeatedSymbol = (string)=> {
            let arr = new Set(string.split(""));
            let result = [];
            arr.forEach(e => {
                if (string.split(e).length - 1 === 2 ) {
                    result.push(e + e);
                }
            });
            if (result.length === 0){
                return 'Ничего'
            } else {
                return result;
            }
        }

        const repeatedSymbolsArr = getRepeatedSymbol(combinationStr);
        console.log(repeatedSymbolsArr)

        if (repeatedSymbolsArr.length === 1){
            return 'Одна пара карт!'
        } else if(repeatedSymbolsArr.length === 2 || repeatedSymbolsArr.length === 3){
            return 'Две пары карт!'
        }
    }
}
const myCardDeck = new CardDeck();
const finalFive = myCardDeck.getCards(5);
console.log(finalFive);
class App extends Component {
    state = {
        // cards:finalFive,
        cards:[{suit:'H', ranks:'7'},{suit:'D', ranks:'7'},{suit:'S', ranks:'7'},{suit:'H', ranks:'8'},{suit:'H', ranks:'3'}]
    }

  render() {
      const pokerH = new PokerHand(this.state.cards);
    return (
        <div className="App">
            <p>{pokerH.getOutcome()}</p>
            <div className='playingCards  faceImages flex'>
              <Cards suit={this.state.cards[0].suit} rank={this.state.cards[0].ranks}/>
              <Cards suit={this.state.cards[1].suit} rank={this.state.cards[1].ranks}/>
              <Cards suit={this.state.cards[2].suit} rank={this.state.cards[2].ranks}/>
              <Cards suit={this.state.cards[3].suit} rank={this.state.cards[3].ranks}/>
              <Cards suit={this.state.cards[4].suit} rank={this.state.cards[4].ranks}/>
            </div>
            <button className='btnNew' onClick={()=>{
            const newCards = new CardDeck();
            const fiveCards = newCards.getCards(5);
            this.setState({cards:fiveCards});
            }
            }>Новые Карты</button>
        </div>
    );
  }
}

export default App;
