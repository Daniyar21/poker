import React from 'react';
import './Cards.css'

const Cards = props => {
    let classSuits = null;

    if (props.suit === "H") {
        classSuits = "hearts";
    } else if (props.suit === "S") {
        classSuits = "spades";
    } else if (props.suit === "C") {
        classSuits = "clubs";
    } else if (props.suit === "D") {
        classSuits = "diams";
    }

    let suitImg = null;

    if (props.suit === "H") {
        suitImg = "♥";
    } else if (props.suit === "S") {
        suitImg = "♠";
    } else if (props.suit === "C") {
        suitImg = "♣";
    } else if (props.suit === "D") {
        suitImg = "♦";
    }

    const cardClass =  'card rank-' + props.rank + ' ' + classSuits;

    return (
        <div className = {cardClass}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{suitImg}</span>
        </div>
    );
};

export default Cards;